from socketserver import DatagramRequestHandler
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.cluster import KMeans
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler
from sklearn import preprocessing
from sklearn.metrics import confusion_matrix,accuracy_score
from sklearn.svm import LinearSVC, SVC
import streamlit as st
import matplotlib.pyplot as plt
from sklearn import svm

#load movies data
patientsData = pd.read_csv('patientsData.csv', delimiter=';')
#remove unnecessary columns
patientsData.drop(["id"], axis = 1, inplace = True)
#convert age days to years
patientsData['age'] = patientsData['age'].div(365).round().astype(int)

def preprocessData():
    le = preprocessing.LabelEncoder()
    age = le.fit_transform(list(patientsData["age"]))
    gender = le.fit_transform(list(patientsData["gender"]))
    height = le.fit_transform(list(patientsData["height"]))
    weight = le.fit_transform(list(patientsData["weight"]))
    ap_hi = le.fit_transform(list(patientsData["ap_hi"]))
    ap_lo = le.fit_transform(list(patientsData["ap_lo"]))
    cholesterol = le.fit_transform(list(patientsData["cholesterol"]))
    gluc = le.fit_transform(list(patientsData["gluc"]))
    smoke = le.fit_transform(list(patientsData["smoke"]))
    alco = le.fit_transform(list(patientsData["alco"]))
    active = le.fit_transform(list(patientsData["active"]))
    cardio = le.fit_transform(list(patientsData["cardio"]))

    features = list(zip(age, gender, height, weight, ap_hi, ap_lo, cholesterol, gluc, smoke, alco, active))
    target = list(cardio)  
    return features, target


def predict(model):
    features, target = preprocessData()
    features_train, features_test, target_train, target_test = train_test_split(features, target, test_size = 0.2)
    sc = StandardScaler()
    features_train = sc.fit_transform(features_train)
    features_test = sc.transform(features_test)
    if model == 1:
        classifier = LinearSVC()
    if model == 2:
        classifier = LogisticRegression()
    if model == 3:
        classifier = KMeans(n_clusters=2, random_state=0)
    
    classifier.fit(features_train, target_train)
    target_predict = classifier.predict(features_test)
    cm = confusion_matrix(target_test, target_predict)
    ac = accuracy_score(target_test,target_predict)
    st.write(cm)
    st.write(ac)

def averageAHW(male):
    if male:
        mean = [patientsData[patientsData["gender"] == 2]["age"].mean(), patientsData[patientsData["gender"] == 2]["height"].mean(), patientsData[patientsData["gender"] == 2]["weight"].mean()]
        feature = ['age', 'height', 'weight']
        fig, ax = plt.subplots()
        ax.bar(feature, mean)
        st.pyplot(fig)
    else:
        mean = [patientsData[patientsData["gender"] == 1]["age"].mean(), patientsData[patientsData["gender"] == 1]["height"].mean(), patientsData[patientsData["gender"] == 1]["weight"].mean()]
        feature = ['age', 'height', 'weight']
        fig, ax = plt.subplots()
        ax.bar(feature, mean)
        st.pyplot(fig)

def averageAHWHILO():
    mean = [patientsData["age"].mean(), patientsData["height"].mean(), patientsData["weight"].mean(), patientsData["ap_hi"].mean(), patientsData["ap_lo"].mean()]
    feature = ['age', 'height', 'weight',"ap_hi" ,"ap_lo"]
    fig, ax = plt.subplots()
    ax.bar(feature, mean)
    st.pyplot(fig)
    
def countSAA():
    count = [patientsData["smoke"].sum(), patientsData["alco"].sum(), patientsData["active"].sum()]
    feature = ['smoke', 'alco', 'active']
    fig, ax = plt.subplots()
    ax.bar(feature, count)
    st.pyplot(fig)
    
def countCG():
    countColesterol = [patientsData[patientsData["cholesterol"] == 1]["cholesterol"].count(), patientsData[patientsData["cholesterol"] == 2]["cholesterol"].count(), patientsData[patientsData["cholesterol"] == 3]["cholesterol"].count(), patientsData[patientsData["gluc"] == 1]["gluc"].count(), patientsData[patientsData["gluc"] == 2]["gluc"].count(), patientsData[patientsData["gluc"] == 3]["gluc"].count()]
    feature = ['col1', 'col2', 'col3','g1', 'g2', 'g3']
    fig, ax = plt.subplots()
    ax.bar(feature, countColesterol)
    st.pyplot(fig)
    
def main():
    st.title('Cardiovascular Disease Prediction App')
    col1, col2, col3, col4, col5 = st.columns(5)
    col6, col7, col8 = st.columns(3)
    with col1:
        AHWM = st.button('Average age, height, weight for men')
    with col2:
        AHWW = st.button('Average age, height, weight for women')
    with col3:
        AHWHILO = st.button('Average age, height, weight, AP_HI, AP_LOW')
    with col4:
        SAA = st.button('Count of smokers, alcoholics and active patients')
    with col5:
        CG = st.button('Cholesterol and Glucose count')
    with col6:
        LinearSVC = st.button('LinearSVC')
    with col7:
        KNN = st.button('Log reg')
    with col8:
        Kmeans = st.button('Kmeans')
        
    if AHWM:
        averageAHW(True)
    if AHWW:
        averageAHW(False)
    if AHWHILO:
        averageAHWHILO()
    if SAA:
        countSAA()
    if CG:
        countCG()
    if LinearSVC:
        predict(1)
    if KNN:
        predict(2)
    if Kmeans:
        predict(3)
    
if __name__ == "__main__":
    main()